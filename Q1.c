#include<stdio.h>
 int main()
 {
     int n1, n2, n3, sum;
     float avg;
     printf("Enter three Numbers one by one: ");
     scanf("%d %d %d", &n1, &n2, &n3);
     sum = n1 + n2 + n3;
     avg = sum / 3;
     printf("Entered numbers are: %d, %d and %d\n", n1, n2, n3);
     printf("Sum=%d\n", sum);
     printf("Average=%.2f\n",avg );
     return 0;
 }
