#include <stdio.h>
int main()
{
	int a=10, b=15;
	printf("Bitwise Operators\nLet's take A=10 and B=15");
	printf("\nA&B = %d", a&b);
	printf("\nA^B = %d", a^b);
	printf("\n~A = %d", ~a);
	printf("\nA<<3 = %d", a<<3);
	printf("\nB>>3 = %d", b>>3);
    return 0;
}
